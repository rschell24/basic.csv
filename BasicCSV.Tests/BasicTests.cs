using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace BasicCSV.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            BasicCSV.Reader csv = new BasicCSV.Reader(@"/home/bob/funfile.csv");
            Assert.True(csv.header.Count == 3);
        }
    }
}