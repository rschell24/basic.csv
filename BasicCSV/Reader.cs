﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BasicCSV
{
    public class Reader
    {
        FileStream fileStream;
        StreamReader streamReader;

        public List<string> header = new List<string>();

        public Reader(string filePath)
        {
            openFile(filePath);
        }

        private bool openFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new ArgumentException("File " + filePath + " not found!");
            }

            try
            {
                fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                streamReader = new StreamReader(fileStream, Encoding.UTF8);
                string headerLine = streamReader.ReadLine();
                string[] columns = headerLine.Split(',');

                foreach(string column in columns)
                {
                    header.Add(column);
                }
            }
            catch(Exception)
            {
                return false;
            }
        
            return true;
        }

        public Dictionary<string, string> nextRecord()
        {
            string line = string.Empty;

            if((line = streamReader.ReadLine()) != null)
            {
                string [] records = line.Split(',');

                Dictionary<string, string> csvRecord = new Dictionary<string, string>();
                for(int i = 0; i < header.Count; i++)
                {
                    csvRecord.Add(header[i], records[i]);
                }

                return csvRecord;
            }

            streamReader.Close();
            fileStream.Close();
            return null;
        }

        ~Reader()
        {
            streamReader.Close();
            fileStream.Close();
        }
    }
}